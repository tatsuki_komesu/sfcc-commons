/**
 * Takes a value with type T, returns a value with type R
 * 型 T の値を受け取り、型 R の値を返す関数。
 * @callback Transform
 * @template T, R
 * @param {T} value
 * @return {R}
 */

/**
 * Takes a value with type T, returns nothing
 * 型 T の値を受け取り、何も返さない関数
 * @callback Handler
 * @template T
 * @param T
 * @return {void}
 */

/**
 * Error class for Result Type
 * Result<T, E>型に関するエラー
 * @type {ResultTypeError}
 */
const ResultTypeError = (function() {
  /**
   * @param {string} message
   * @constructor
   */
  function ResultTypeError(message) {
    this.name = 'ResultTypeError';
    this.message = message || 'Result<T, E> Typing Error';
  }
  ResultTypeError.prototype = new Error();
  ResultTypeError.prototype.constructor = ResultTypeError;
  return ResultTypeError;
})();

/**
 * One for succeeded result of the two variation of Result<T, E>.
 * Result<T, E>型で処理が成功していたときの実際のオブジェクト
 * @template T
 * @param {T} value
 * @constructor
 */
function Ok(value) {
  this.__value__ = value;
}

/**
 * Returns the process has been succeeded or not.
 * 処理が成功していたかどうかを返す
 * @template T, E
 * @this {Ok<T>|Err<E>}
 * @return {boolean}
 */
Ok.prototype.isOk = function isOk() {
  return true;
};

/**
 * Returns the process has been failed or not.
 * 処理が失敗したかどうかを返す
 * @template T, E
 * @this {Ok<T>|Err<E>}
 * @return {boolean}
 */
Ok.prototype.isErr = function isErr() {
  return false;
};

/**
 * One for failed result of the two variation of Result<T, E>.
 * Result<T, E>型で処理が失敗していた場合の実際のオブジェクト
 * @template E
 * @param {E} error
 * @constructor
 */
function Err(error) {
  this.__error__ = error;
}

/**
 * Returns the process has been succeeded or not.
 * @template T, E
 * @this {Ok<T>|Err<E>}
 * @return {boolean}
 */
Err.prototype.isOk = function isOk() {
  return false;
};

/**
 * Returns the process has been failed or not.
 * @template T, E
 * @this {Ok<T>|Err<E>}
 * @return {boolean}
 */
Err.prototype.isErr = function isErr() {
  return true;
};

/**
 * Construct Ok<T> Result
 * @template T
 * @param {T} value
 * @return {Ok<T>}
 */
function ok(value) {
  return new Ok(value);
}

/**
 * Construct Error<E> Result
 * @template E
 * @param {E} error
 * @return {Err<E>}
 */
function err(error) {
  return new Err(error);
}

/**
 * Does some procedures for inner value and returns Ok<R>|Err<E2>.
 * It does nothing and just return Err itself for Error result.
 * 内部の成功値を別の Ok値またはErr値に変換する。
 * もともとオブジェクト自体がErr値だった場合は、内部の成功値が存在しないためそのErrオブジェクトをそのまま返却する。
 * @template T, E1, R, E2
 * @this {Ok<T>|Err<E1>}
 * @param {Transform<T, R>|Transform<T, Ok<R>|Err<E2>>} transform is a function which takes succeeded raw result data and should return Ok<R>|Err<E2>.
 *  Ok時の内部値を受け取り、新しいOk値かErr値を返却するコールバック関数
 * @return {Ok<R>|Err<E1|E2>}
 */
Ok.prototype.then = function then(transform) {
  const result = transform(this.__value__);
  if (result instanceof Ok || result instanceof Err) {
    return result;
  }
  return ok(result);
};

/**
 * Does some procedures for inner succeeded value and returns Ok<R>|Err<E2>.
 * It does nothing and just return Err itself for Error result.
 * 内部の成功値を元に別のOk値またはErr値に変換するためのメソッド。
 * もともとオブジェクト自体がErr値だった場合は、内部の成功値が存在しないためそのErrオブジェクトをそのまま返却する。
 * @template T, E1, R, E2
 * @this {Ok<T>|Err<E1>}
 * @param {Transform<T, R>|Transform<T, Ok<R>|Err<E2>>} _transform is a function which takes succeeded raw result data and should return Ok<R>|Err<E2>.
 *  Ok時の内部値を受け取り、新しいOk値かErr値を返却するコールバック関数
 * @return {Ok<R>|Err<E1|E2>}
 */
Err.prototype.then = function then(_transform) {
  return this;
};

/**
 * Does some procedures for inner error value and returns Ok<T>|Err<E2>.
 * It does nothing for Ok result and just return Ok itself.
 * エラーだった場合に、エラー値をもとに別のエラーに置き換えたり、回復可能なエラーの場合は何かしらのデフォルト値が入ったOk値に置き換えるメソッド。
 * もともとOk値の場合は何もせずにそのOk値を返却する。
 * @template T, E1, E2
 * @this {Ok<T>|Err<E1>}
 * @param {Transform<E1, T>|Transform<E1, Ok<T>|Err<E2>>} _transform
 * @return {Ok<T>|Err<E2>}
 */
Ok.prototype.catch = function _catch(_transform) {
  return this;
};

/**
 * Does some procedures for inner error value and returns Ok<R>|Err<E2>.
 * It does nothing for Ok result and just return Ok itself.
 * エラーだった場合に、エラー値をもとに別のエラーに置き換えたり、回復可能なエラーの場合は何かしらのデフォルト値が入ったOk値に置き換えるメソッド。
 * もともとOk値の場合は何もせずにそのOk値を返却する。
 * @template T, E1, E2
 * @this {Ok<T>|Err<E1>}
 * @param {Transform<E1, T>|Transform<E1, Ok<T>|Err<E2>>} transform
 * @return {Ok<T>|Err<E2>}
 */
Err.prototype.catch = function _catch(transform) {
  const transformed = transform(this.__error__);
  if (transformed instanceof Ok || transformed instanceof Err) {
    return transformed;
  }
  return ok(transformed);
};

/**
 * Does some specific process with ok result.
 * Does nothing for Err.
 * Ok値だった場合、その内部値を使って何か特別な処理をするためのメソッド。
 * Err値だった場合はなにもしない。
 * @template T, E
 * @this {Ok<T>|Err<E>}
 * @param {Handler<T>} handler takes successful result value and does some procedure.
 * @return {Ok<T>|Err<E>}
 */
Ok.prototype.tap = function tap(handler) {
  handler(this.__value__);
  return this;
};

/**
 * Does some specific process with ok result inner value.
 * Does nothing for Err.
 * Ok値だった場合、その内部値を使って何か特別な処理をするためのメソッド。
 * Err値だった場合はなにもしない。
 * @template T, E
 * @this {Ok<T>|Err<E>}
 * @param {Handler<T>} _handler takes successful result value and does some procedure.
 * @return {Ok<T>|Err<E>}
 */
Err.prototype.tap = function tap(_handler) {
  return this;
};

/**
 * Does some specific process with err result inner value.
 * Does nothing for Ok.
 * Err値だった場合、その内部のエラー値を使って何か特別な処理をするためのメソッド。
 * Ok値だった場合はなにもしない。
 * @template T, E
 * @this {Ok<T>|Err<E>}
 * @param {Handler<T>} _handler takes successful result value and does some procedure.
 * @return {Ok<T>|Err<E>}
 */
Ok.prototype.tapError = function tapError(_handler) {
  return this;
};

/**
 * Does some specific process with err result inner value.
 * Does nothing for Ok.
 * Err値だった場合、その内部のエラー値を使って何か特別な処理をするためのメソッド。
 * Ok値だった場合はなにもしない。
 * @template T, E
 * @this {Ok<T>|Err<E>}
 * @param {Handler<T>} handler takes successful result value and does some procedure.
 * @return {Ok<T>|Err<E>}
 */
Err.prototype.tapError = function tapError(handler) {
  handler(this.__error__);
  return this;
};

/**
 * Returns inner value of Ok result, if Err, returns given default value instead.
 * Use only if all possible errors can be recovered by just using default value.
 * Ok値の内部値を取り出すためのメソッド。Err値の場合は、与えられたデフォルト値を使用する。
 * 考え得るあらゆるエラーが「単にデフォルト値を使用する」ことで回復可能な場合に使用する。
 * @template T, E
 * @this {Ok<T>|Err<E>}
 * @param {T} _defaultValue
 * @return {T}
 */
Ok.prototype.getOrDefault = function getOrDefault(_defaultValue) {
  return this.__value__;
};

/**
 * Returns inner value of Ok result, if Err, returns given default value instead.
 * Use only if all possible errors can be recovered by just using default value.
 * Ok値の内部値を取り出すためのメソッド。Err値の場合は、与えられたデフォルト値を使用する。
 * 考え得るあらゆるエラーが「単にデフォルト値を使用する」ことで回復可能な場合に使用する。
 * @template T, E
 * @this {Ok<T>|Err<E>}
 * @param {T} defaultValue
 * @return {T}
 */
Err.prototype.getOrDefault = function getOrDefault(defaultValue) {
  return defaultValue;
};

/**
 * Returns inner value of Ok result, if Err, returns the default value calculated by given callback.
 * Use only if all possible errors can be recovered by some calculated default value, or if you want the program just to crush if error occurred.
 * Ok値の内部値を取り出すためのメソッド。Err値の場合は、与えられたコールバックの実行結果を代わりのデフォルト値として取得する。
 * 考え得るすべてのエラーが何らかのデフォルト値の計算で回復可能な場合、もしくは「ここでエラーになった場合は即例外でプログラムが落ちてほしい」場合に例外を投げる関数を与えて使用する。
 * @template T, E
 * @this {Ok<T>|Err<E>}
 * @param {Transform<E, T>} _transform
 * @return {T}
 */
Ok.prototype.getOrElse = function getOrElse(_transform) {
  return this.__value__;
};

/**
 * Returns inner value of Ok result, if Err, returns the default value calculated by given callback.
 * Use only if all possible errors can be recovered by some calculated default value, or if you want the program just to crush if error occurred.
 * Ok値の内部値を取り出すためのメソッド。Err値の場合は、与えられたコールバックの実行結果を代わりのデフォルト値として取得する。
 * 考え得るすべてのエラーが何らかのデフォルト値の計算で回復可能な場合、もしくは「ここでエラーになった場合は即例外でプログラムが落ちてほしい」場合に例外を投げる関数を与えて使用する。
 * @template T, E
 * @this {Ok<T>|Err<E>}
 * @param {Transform<E, T>} transform
 * @return {T}
 */
Err.prototype.getOrElse = function getOrElse(transform) {
  return transform(this.__error__);
};

/**
 * Take inner error value. Throws exception if Err.
 * 内部のエラー値を取得する関数。Ok値だった場合は即座に例外を投げる。
 * @template T, E
 * @this {Ok<T>|Err<E>}
 * @throws {ResultTypeError}
 * @return {E}
 */
Ok.prototype.takeError = function takeError() {
  throw new ResultTypeError(
    'takeError() called for Ok type. Check Result#isErr() before take inner error.'
  );
};

/**
 * Take inner error value. Throws exception if Err.
 * 内部のエラー値を取得する関数。Ok値だった場合は即座に例外を投げる。
 * @template T, E
 * @this {Ok<T>|Err<E>}
 * @return {E}
 */
Err.prototype.takeError = function takeError() {
  return this.__error__;
};

/**
 * @typedef ResultMatcher
 * @template T, E, R
 * @property {Transform<T, R>} Ok
 * @property {Transform<E, R>} Err
 */

/**
 * Obtain some value for each pattern.
 * [important] The both functions must return values with same type, or Err-side function must always throw exception.
 * Ok, Errだった場合のそれぞれの処理結果を得る関数。
 * 重要: ふたつの関数が同じ型の値を返すか、Err側が例外を投げる関数であること。
 * @template T, E, R
 * @this {Ok<T>|Err<E>}
 * @param {ResultMatcher<T, E, R>} matcher
 * @return {R}
 */
Ok.prototype.match = function match(matcher) {
  if (typeof matcher.Ok !== 'function' || typeof matcher.Err !== 'function') {
    throw new ResultTypeError(
      'You must give two function property Ok and Err for Result#match'
    );
  }
  return matcher.Ok(this.__value__);
};

/**
 * Obtain some value for each pattern.
 * [important] The both functions must return values with same type, or Err-side function must always throw exception.
 * Ok, Errだった場合のそれぞれの処理結果を得る関数。
 * 重要: ふたつの関数が同じ型の値を返すか、Err側が例外を投げる関数であること。
 * @template T, E, R
 * @this {Ok<T>|Err<E>}
 * @param {ResultMatcher<T, E, R>} matcher
 * @return {R}
 */
Err.prototype.match = function match(matcher) {
  if (typeof matcher.Ok !== 'function' || typeof matcher.Err !== 'function') {
    throw new ResultTypeError(
      'You must give two function property Ok and Err for Result#match'
    );
  }
  return matcher.Err(this.__error__);
};

module.exports = {
  Ok: ok,
  Err: err,
  ResultTypeError: ResultTypeError
};
