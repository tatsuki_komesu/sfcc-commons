/**
 * Takes a value with type T, returns nothing
 * 型 T の値を受け取り、何も返さない関数
 * @callback Handler
 * @template T
 * @param T
 * @return {void}
 */

/**
 * Takes nothing, returns nothing. Just makes side effect
 * 何も引数を取らず、何も返さず副作用の処理だけをする関数
 * @callback Process
 * @return {void}
 */

/**
 * Takes nothing, returns a value with type T
 * @callback Generate
 * @template T
 * @return {T}
 */

/**
 * Error class for Option Type
 * Option<T>型に関するエラー
 * @type {OptionTypeError}
 */
const OptionTypeError = (function() {
  /**
   * @param {string} message
   * @constructor
   */
  function OptionTypeError(message) {
    this.name = 'OptionTypeError';
    this.message = message || 'Option<T> Typing Error';
  }
  OptionTypeError.prototype = new Error();
  OptionTypeError.prototype.constructor = OptionTypeError;
  return OptionTypeError;
})();

/**
 * One for existing value of the two variation of Option<T>.
 * 値が存在する場合のバリエーション
 * @template T
 * @param {T} value
 * @constructor
 */
function Some(value) {
  this.__value__ = value;
}

/**
 * Returns the value actually exists.
 * 値が存在するかどうかを返す。
 * @template T
 * @this {Some<T>|None}
 * @return {boolean}
 */
Some.prototype.isSome = function isSome() {
  return true;
};

/**
 * Returns the value actually doesn't exist.
 * 値が不存在なのかどうかを返す。
 * @template T
 * @this {Some<T>|None}
 * @return {boolean}
 */
Some.prototype.isNone = function isNone() {
  return false;
};

/**
 * One for nothing of the two variation of Option<T>.
 * 値が実際には存在しない場合のオブジェクト
 * @constructor
 */
function None() {}

const _none = new None();

/**
 * Returns the value actually exists.
 * 値が存在するかどうかを返す。
 * @template T
 * @this {Some<T>|None}
 * @return {boolean}
 */
None.prototype.isSome = function isSome() {
  return false;
};

/**
 * Returns the value actually doesn't exist.
 * 値が不存在なのかどうかを返す。
 * @template T
 * @this {Some<T>|None}
 * @return {boolean}
 */
None.prototype.isNone = function isNone() {
  return true;
};

/**
 * Construct Some<T>|None
 * Some値かNone値を構築する。nullやundefinedに対してはNoneを返却する。
 * @template T
 * @param {T} value
 * @return {Some<T>|None}
 */
function option(value) {
  if (value === null || value === undefined) {
    return _none;
  }
  return new Some(value);
}

/**
 * Returns None
 * None を返却する。
 * @template E
 * @return {None}
 */
function none() {
  return _none;
}

/**
 * @callback Transform
 * @template T, R
 * @param {T} value
 * @return {R}
 */

/**
 * Does some procedures for inner value and returns Some<R>|None.
 * It does nothing and just return None itself for None.
 * 内部値を別のSome値またはNone値に変換する。
 * もともとがNone値だった場合は、内部値が存在しないためNoneを返却する・。
 * @template T, R
 * @this {Some<T>|None}
 * @param {Transform<T, R>|Transform<T, Some<R>|None>} transform is a function which takes inner value of this and should return Some<R>|None.
 *  Some時の内部値を受け取り、新しいSome値かNone値を返却するコールバック関数
 * @return {Some<R>|None}
 */
Some.prototype.then = function then(transform) {
  const result = transform(this.__value__);
  if (result instanceof Some || result instanceof None) {
    return result;
  }
  return option(result);
};

/**
 * Does some procedures for inner value and returns Some<R>|None.
 * It does nothing and just return None itself for None.
 * 内部値を別のSome値またはNone値に変換する。
 * もともとがNone値だった場合は、内部値が存在しないためNoneを返却する・。
 * @template T, R
 * @this {Some<T>|None}
 * @param {Transform<T, R>|Transform<T, Some<R>|None>} _transform is a function which takes inner value of this and should return Some<R>|None.
 *  Some時の内部値を受け取り、新しいSome値かNone値を返却するコールバック関数
 * @return {Some<R>|None}
 */
None.prototype.then = function then(_transform) {
  return this;
};

/**
 * Generates a Option<T> value and returns it if None.
 * If Some, just return it.
 * Use this if you want a default value calculated by a function, but it may be None again.
 * Noneだった場合に、与えられた関数の実行結果をOptionで包んだ状態で返す。
 * Someだった場合はその値をそのまま返す。
 * デフォルト値がほしいが、そのデフォルト値の計算もNoneになる可能性がある場合などに使用する。
 * @template T
 * @this {Some<T>|None}
 * @param {Generate<T|Some<T>|None>} _generate
 * @return {Some<T>|None}
 */
Some.prototype.forNone = function forNone(_generate) {
  return this;
};

/**
 * Generates a Option<T> value and returns it if None.
 * If Some, just return it.
 * Use this if you want a default value calculated by a function, but it may be None again.
 * Noneだった場合に、与えられた関数の実行結果をOptionで包んだ状態で返す。
 * Someだった場合はその値をそのまま返す。
 * デフォルト値がほしいが、そのデフォルト値の計算もNoneになる可能性がある場合などに使用する。
 * @template T
 * @this {Some<T>|None}
 * @param {Generate<T|Some<T>|None>} generate
 * @return {Some<T>|None}
 */
None.prototype.forNone = function forNone(generate) {
  const generated = generate();
  if (generated instanceof Some || generated instanceof None) {
    return generated;
  }
  return option(generated);
};

/**
 * @template T
 * @this {Some<T>|None}
 * @param {Process} _procedure
 * @return {Some<T>|None}
 */
Some.prototype.doIfNone = function doIfNone(_procedure) {
  return this;
};

/**
 * @template T
 * @this {Some<T>|None}
 * @param {Handler<E>} procedure
 * @return {Some<T>|None}
 */
None.prototype.doIfNone = function doIfNone(procedure) {
  procedure(this.__error__);
  return this;
};

/**
 * Does some specific process with Some result.
 * Does nothing for None.
 * Some値だった場合、その内部値を使って何か特別な処理をするためのメソッド。
 * None値だった場合はなにもしない。
 * @template T
 * @this {Some<T>|None}
 * @param {Handler<T>} handler takes successful result value and does some procedure.
 * @return {Some<T>|None}
 */
Some.prototype.tap = function tap(handler) {
  handler(this.__value__);
  return this;
};

/**
 * Does some specific process with Some result inner value.
 * Does nothing for None.
 * Some値だった場合、その内部値を使って何か特別な処理をするためのメソッド。
 * None値だった場合はなにもしない。
 * @template T
 * @this {Some<T>|None}
 * @param {Handler<T>} _handler takes successful result value and does some procedure.
 * @return {Some<T>|None}
 */
None.prototype.tap = function tap(_handler) {
  return this;
};

/**
 * Does some specific process if this object is None.
 * Does nothing for Some.
 * None値だった場合、何か特別な処理をするためのメソッド。
 * Some値だった場合はなにもしない。
 * @template T
 * @this {Some<T>|None}
 * @param {Handler<T>} _handler takes successful result value and does some procedure.
 * @return {Some<T>|None}
 */
Some.prototype.doIfNone = function doIfNone(_handler) {
  return this;
};

/**
 * Does some specific process if this object is None.
 * Does nothing for Some.
 * None値だった場合、何か特別な処理をするためのメソッド。
 * Some値だった場合はなにもしない。
 * @template T
 * @this {Some<T>|None}
 * @param {Handler<T>} handler takes successful result value and does some procedure.
 * @return {Some<T>|None}
 */
None.prototype.doIfNone = function doIfNone(handler) {
  handler();
  return this;
};

/**
 * Returns inner value of Some result, if None, returns given default value instead.
 * Some値の内部値を取り出すためのメソッド。None値の場合は、与えられたデフォルト値を使用する。
 * @template T
 * @this {Some<T>|None}
 * @param {T} _defaultValue
 * @return {T}
 */
Some.prototype.getOrDefault = function getOrDefault(_defaultValue) {
  return this.__value__;
};

/**
 * Returns inner value of Some result, if None, returns given default value instead.
 * Some値の内部値を取り出すためのメソッド。None値の場合は、与えられたデフォルト値を使用する。
 * @template T
 * @this {Some<T>|None}
 * @param {T} defaultValue
 * @return {T}
 */
None.prototype.getOrDefault = function getOrDefault(defaultValue) {
  return defaultValue;
};

/**
 * Returns inner value of Some result, if None, returns given default value calculated by given callback instead.
 * Some値の内部値を取り出すためのメソッド。None値の場合は、与えられた関数から計算したデフォルト値を使用する。
 * @template T
 * @this {Some<T>|None}
 * @param {Generate<T>} _generate
 * @return {T}
 */
Some.prototype.getOrElse = function getOrElse(_generate) {
  return this.__value__;
};

/**
 * Returns inner value of Some result, if None, returns given default value calculated by given callback instead.
 * Some値の内部値を取り出すためのメソッド。None値の場合は、与えられた関数から計算したデフォルト値を使用する。
 * @template T
 * @this {Some<T>|None}
 * @param {Generate<T>} generate
 * @return {T}
 */
None.prototype.getOrElse = function getOrElse(generate) {
  return generate();
};

/**
 * @typedef OptionMatcher
 * @template T, R
 * @property {Transform<T, R>} Some
 * @property {Generate<R>} None
 */

/**
 * Obtain some value for each pattern.
 * [important] The both functions must return values with same type, or None-side function must always throw exception.
 * Some, Noneだった場合のそれぞれの処理結果を得る関数。
 * 重要: ふたつの関数が同じ型の値を返すか、None側が例外を投げる関数であること。
 * @template T,  R
 * @this {Some<T>|None}
 * @param {OptionMatcher<T, R>} matcher
 * @return {R}
 */
Some.prototype.match = function match(matcher) {
  if (typeof matcher.Some !== 'function' || typeof matcher.None !== 'function') {
    throw new OptionTypeError(
        'You must give two function property Some and None for Option#match'
    );
  }
  return matcher.Some(this.__value__);
};

/**
 * Obtain some value for each pattern.
 * [important] The both functions must return values with same type, or None-side function must always throw exception.
 * Some, Noneだった場合のそれぞれの処理結果を得る関数。
 * 重要: ふたつの関数が同じ型の値を返すか、None側が例外を投げる関数であること。
 * @template T, R
 * @this {Some<T>|None}
 * @param {OptionMatcher<T, R>} matcher
 * @return {R}
 */
None.prototype.match = function match(matcher) {
  if (typeof matcher.Some !== 'function' || typeof matcher.None !== 'function') {
    throw new OptionTypeError(
      'You must give two function property Some and None for Option#match'
    );
  }
  return matcher.None();
};

module.exports = {
  Option: option,
  None: none,
  OptionTypeError: OptionTypeError
};
